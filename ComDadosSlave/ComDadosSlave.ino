//#define RXD2 16  //Pino RX caso nao seja utilizado o pino padrao(GPIO1)
//#define TXD2 17  //Pino TX caso nao seja utilizado o pino padrao(GPIO3)

//SLAVE-1

#include <HardwareSerial.h>

HardwareSerial mySerial2(2);

#define RXD2 16  //Pino RX caso nao seja utilizado o pino padrao(GPIO1)
#define TXD2 17  //Pino TX caso nao seja utilizado o pino padrao(GPIO3)
#define PT100 14 //Pino do pt100 (0 ate 4096 com 0 a 3.0v)

#define LED1 33 //Pino led ON/OFF (Sempre ligado caso slave esteja ligado)
#define LED2 25 //Led indica uso do barramento
#define LED3 26 //Led Capturando sensor
#define LED4 27 //Led indica se ultrapassou  SP(Liga se o valor lido>SP)


int i=0;
char FrameEnv[7]={'1','0','0','0','0','0','\0'};
char receb[7]={'0','0','0','0','0','0','\0'};
int leituraADC=0;
char dados4bytes[5]={'0','0','0','0','\0'};
char SPchar[5]={'0','0','0','0','\0'};
int SP=0;

void setup() {
  // Note the format for setting a serial port is as follows: Serial2.begin(baud-rate, protocol, RX pin, TX pin);
  Serial.begin(115200);
  mySerial2.begin(9600, SERIAL_8E2, RXD2, TXD2);
  //mySerial2.begin(9600, SERIAL_8N1);  //RX e TX padrão
  Serial.println("Serial Txd is on pin: "+String(TXD2));
  Serial.println("Serial Rxd is on pin: "+String(RXD2));
  pinMode(23, OUTPUT);
  pinMode(PT100, INPUT);
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(LED4, OUTPUT);

  digitalWrite(LED1, HIGH);
}

void loop() { 
  digitalWrite(LED3, LOW);
  digitalWrite(23, LOW);
  delay(10);

  if(mySerial2.available()){
    i=0;
    while (mySerial2.available()) {
      receb[i]=((mySerial2.read()));
      i++;
    }
    if(receb[1]=='1' && receb[0]=='0') //Verifica se recebido é para SLAVE-1
    {
      digitalWrite(LED3, HIGH);
      Serial.print("Recebido do disp. ");
      Serial.print(receb[0]);
      Serial.print(": ");
      Serial.println(receb);

      SPchar[0]=receb[2]; SPchar[1]=receb[3]; SPchar[2]=receb[4]; SPchar[3]=receb[5];
      SP=atoi(SPchar);
      
      
      leituraADC=analogRead(PT100);
      itoa(leituraADC, dados4bytes, 10);
      Serial.println(leituraADC);
      if(dados4bytes[3]!='\0'){
        FrameEnv[2]=dados4bytes[0]; FrameEnv[3]=dados4bytes[1]; FrameEnv[4]=dados4bytes[2]; FrameEnv[5]=dados4bytes[3];
      }
      else if(dados4bytes[3]=='\0'){
        FrameEnv[2]='0'; FrameEnv[3]=dados4bytes[0]; FrameEnv[4]=dados4bytes[1]; FrameEnv[5]=dados4bytes[2];
      }
      
      if(leituraADC>SP){
        digitalWrite(LED4, HIGH);
      }
      else{
        digitalWrite(LED4, LOW);
      }
      
      digitalWrite(23, LOW);
      digitalWrite(LED2, HIGH);
      digitalWrite(23, HIGH); //Seta para enviar pelo mySerial2
      mySerial2.print(FrameEnv); //Envia pelo mySerial2
      delay(10);
      digitalWrite(LED2, LOW);
    }
    
  } 
}
