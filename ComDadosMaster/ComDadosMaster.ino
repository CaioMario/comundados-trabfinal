
#include <HardwareSerial.h>
#define RXD2 16  //Pino RX caso nao seja utilizado o pino padrao(GPIO1)
#define TXD2 17  //Pino TX caso nao seja utilizado o pino padrao(GPIO3)

HardwareSerial mySerial2(2);

int i=0, j=0;
//Na Frame de dados o primeiro bit é quem esta mandando e segundo byte é cabeçalho que identifica quem vai receber a mensagem.
//0x00=Mestre, 0x01=slave1, 0x02=slave2.
char FrameEnv[7]={'0','0','0','0','0','0','\0'}; 
char receb[7]={'0','0','0','0','0','0','\0'};
char dadosReceb[5]={'0','0','0','0','\0'};
char x[5]={'0','0','0','0','0'};
int auxiliar=0;

void setup() {
  // Note the format for setting a serial port is as follows: Serial2.begin(baud-rate, protocol, RX pin, TX pin);
  Serial.begin(115200);
  mySerial2.begin(9600, SERIAL_8E2, RXD2, TXD2);//SERIAL_8E2 significa que transmição de 8bits com dois bits de stop e paridade par(realizada em hardware)
  //mySerial2.begin(9600, SERIAL_8N1);  //RX e TX padrão
  Serial.println("Serial Txd is on pin: "+String(TXD2));
  Serial.println("Serial Rxd is on pin: "+String(RXD2));
  pinMode(15, OUTPUT);
  Serial.setTimeout(200); 
}

void loop() {
  delay(500); 
  digitalWrite(15, HIGH);
  if (Serial.available() > 0) {
    j=0;
    while(Serial.available()){
      x[j] = Serial.read();     //Deve ser passado um numero de 5 digitos pela serial read.
                                // o primeiro será o numero do slave, e o resto será o SP
      j++;
    }
    if(x[0]=='1'||x[0]=='2'||x[0]=='0')
    { 
      auxiliar=0;
      FrameEnv[1]=x[0];
      FrameEnv[2]=x[1];
      FrameEnv[3]=x[2];
      FrameEnv[4]=x[3];
      FrameEnv[5]=x[4];
    }
    
  }
  if(x[0]=='3' || auxiliar==1){
      auxiliar=1;
      if(FrameEnv[1]=='0'){FrameEnv[1]='2';} 
      else if(FrameEnv[1]=='1'){FrameEnv[1]='2';} 
      else if(FrameEnv[1]=='2'){FrameEnv[1]='1';}
      
  }
 // FrameEnv[1]=Serial.read();
  delay(10);
  if(FrameEnv[1]=='1' || FrameEnv[1]=='2') 
  {
    
    mySerial2.print(FrameEnv);
    delay(10);
    digitalWrite(15, LOW);
    delay(20);

    if(mySerial2.available()){
      i=0;
      while (mySerial2.available()) {
        receb[i]=((mySerial2.read()));
        i++;
      }
      if(receb[0]== FrameEnv[1] && receb[1]=='0')
      {
          Serial.println();
          Serial.print("Resposta do dispositivo " );
          Serial.print(FrameEnv[1]);
          Serial.print(": ");
         
          dadosReceb[0]=receb[2]; dadosReceb[1]=receb[3]; dadosReceb[2]=receb[4]; dadosReceb[3]=receb[5];
          int in = ((float)atof(dadosReceb));
          Serial.print(in);
      }
    }


    
  }
}

//void serialEvent(){
 // FrameEnv[1]=Serial.read();
  //}



/* Baud-rates available: 300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 28800, 38400, 57600, or 115200, 256000, 512000, 962100
 *  
 *  Protocols available:
 * SERIAL_5N1   5-bit No parity 1 stop bit
 * SERIAL_6N1   6-bit No parity 1 stop bit
 * SERIAL_7N1   7-bit No parity 1 stop bit
 * SERIAL_8N1 (the default) 8-bit No parity 1 stop bit 
 * SERIAL_5N2   5-bit No parity 2 stop bits 
 * SERIAL_6N2   6-bit No parity 2 stop bits
 * SERIAL_7N2   7-bit No parity 2 stop bits
 * SERIAL_8N2   8-bit No parity 2 stop bits 
 * SERIAL_5E1   5-bit Even parity 1 stop bit
 * SERIAL_6E1   6-bit Even parity 1 stop bit
 * SERIAL_7E1   7-bit Even parity 1 stop bit 
 * SERIAL_8E1   8-bit Even parity 1 stop bit 
 * SERIAL_5E2   5-bit Even parity 2 stop bit 
 * SERIAL_6E2   6-bit Even parity 2 stop bit 
 * SERIAL_7E2   7-bit Even parity 2 stop bit  
 * SERIAL_8E2   8-bit Even parity 2 stop bit it is done in hardware; the appropriate USART Parity Error bit is set and while the datasheet is vague about it, it is implied that the data is stored in the receive buffer. 
 * SERIAL_5O1   5-bit Odd  parity 1 stop bit  
 * SERIAL_6O1   6-bit Odd  parity 1 stop bit   
 * SERIAL_7O1   7-bit Odd  parity 1 stop bit  
 * SERIAL_8O1   8-bit Odd  parity 1 stop bit   
 * SERIAL_5O2   5-bit Odd  parity 2 stop bit   
 * SERIAL_6O2   6-bit Odd  parity 2 stop bit    
 * SERIAL_7O2   7-bit Odd  parity 2 stop bit    
 * SERIAL_8O2   8-bit Odd  parity 2 stop bit    
*/
