
#include <HardwareSerial.h>
// Load Wi-Fi library
#include <WiFi.h>


#define RXD2 16  //Pino RX caso nao seja utilizado o pino padrao(GPIO1)
#define TXD2 17  //Pino TX caso nao seja utilizado o pino padrao(GPIO3)

HardwareSerial mySerial2(2);

// Replace with your network credentials
const char* ssid     = "Virus";
const char* password = "12345678";

// Set web server port number to 80
WiFiServer server(80);
// Variable to store the HTTP request
String header;

// Auxiliar variables to store the current output state
String output0State = "off";
String output1State = "off";
String output2State = "off";
String output3State = "off";

int i=0, j=0;
//Na Frame de dados o primeiro bit é quem esta mandando e segundo byte é cabeçalho que identifica quem vai receber a mensagem.
//0x00=Mestre, 0x01=slave1, 0x02=slave2.
char FrameEnv[7]={'0','0','0','0','0','0','\0'}; 
char receb[7]={'0','0','0','0','0','0','\0'};
char dadosReceb[5]={'0','0','0','0','\0'};
char x[5]={'0','0','0','0','0'};
int in=0;
int auxiliar=1;
String dadosstring, dadosstring1, dadosstring2;

void setup() {
  // Note the format for setting a serial port is as follows: Serial2.begin(baud-rate, protocol, RX pin, TX pin);
  Serial.begin(115200);

  // Connect to Wi-Fi network with SSID and password
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();
  //
  mySerial2.begin(9600, SERIAL_8E2, RXD2, TXD2);//SERIAL_8E2 significa que transmição de 8bits com dois bits de stop e paridade par(realizada em hardware)
  //mySerial2.begin(9600, SERIAL_8N1);  //RX e TX padrão
  Serial.println("Serial Txd is on pin: "+String(TXD2));
  Serial.println("Serial Rxd is on pin: "+String(RXD2));
  pinMode(23, OUTPUT);
  Serial.setTimeout(200); 
}

void loop() {

  WiFiClient client = server.available();   // Listen for incoming clients

  if (client) {                             // If a new client connects,
    Serial.println("New Client.");          // print a message out in the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            
            // turns the GPIOs on and off
            if ((header.indexOf("GET /1/on") >= 0)) {
              output0State = "off";
              output1State = "on";
              output2State = "off";
              output3State = "off";
              x[0]='1';
              dadosstring="0";
            } else if ((header.indexOf("GET /2/on") >= 0)) {
              output0State = "off";
              output1State = "off";
              output2State = "on";
              output3State = "off";
              x[0]='2';
              dadosstring="0";
            } else if ((header.indexOf("GET /3/on") >= 0)) {
              output0State = "off";
              output1State = "off";
              output2State = "off";
              output3State = "on";
              x[0]='3';
              dadosstring="0";
            } else if ((header.indexOf("GET /0/on") >= 0)) {
              output0State = "on";
              output1State = "off";
              output2State = "off";
              output3State = "off";
              x[0]='0';
              dadosstring="0";
            } 
            

            FrameEnv[1]=x[0];
            if(x[0]!='3'){auxiliar=0;}

                      
            if(x[0]=='3' || auxiliar==1){
                auxiliar=1;
                if(FrameEnv[1]=='3'){FrameEnv[1]='2';} 
                if(FrameEnv[1]=='1'){FrameEnv[1]='2';} 
                if(FrameEnv[1]=='2'){FrameEnv[1]='1';}
                Serial.println(FrameEnv[1]);    
            }
            
            digitalWrite(23, HIGH);
            delay(10);
            if(FrameEnv[1]=='1' || FrameEnv[1]=='2') 
            {
              mySerial2.print(FrameEnv);
              delay(10);
              digitalWrite(23, LOW);
              delay(20);
          
              if(mySerial2.available()){
                i=0;
                while (mySerial2.available()) {
                  receb[i]=((mySerial2.read()));
                  i++;
                }
                if(receb[0]== FrameEnv[1] && receb[1]=='0')
                {
                    Serial.println();
                    Serial.print("Resposta do dispositivo " );
                    Serial.print(FrameEnv[1]);
                    Serial.print(": ");
                   
                    dadosReceb[0]=receb[2]; dadosReceb[1]=receb[3]; dadosReceb[2]=receb[4]; dadosReceb[3]=receb[5];
                    in = ((float)atof(dadosReceb));
                    String str(dadosReceb);
                    dadosstring=str;
                    if(FrameEnv[1]=='1'){
                      dadosstring1=dadosstring;
                    }
                    else if(FrameEnv[1]=='2'){
                       dadosstring2=dadosstring;
                    }
                    Serial.print(in);
                }
              }  
            }

            // Display the HTML web page
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.print("<meta http-equiv=\"refresh\" content=\"3\">");
            client.println("<link rel=\"icon\" href=\"data:,\">");
            // CSS to style the on/off buttons 
            // Feel free to change the background-color and font-size attributes to fit your preferences
            client.println("<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}");
            client.println(".button { background-color: #4CAF50; border: none; color: white; padding: 16px 40px;");
            client.println("text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}");
            client.println(".button2 {background-color: #555555;}</style></head>");
            
            // Web Page Heading
            client.println("<body><h1>ESP32 Web Server</h1>");
            
            // Display current state, and ON/OFF buttons for slave 1
            client.println("<p>Slave-1 - " + output1State + "</p>");
            // If the output1State is off, it displays the ON button       
            if (output1State=="off") {
              client.println("<p><a href=\"/1/on\"><button class=\"button\">ON</button></a></p>");
            } else if(output3State=="on"){
              //client.println("<p><a href=\"/3/on\"><button class=\"button\">ON</button></a></p>");
              //client.println("<p>" + dadosstring + "</p>");
            }
            else {
              client.println("<p><a href=\"/1/off\"><button class=\"button button2\">OFF</button></a></p>");
              client.println("<p>" + dadosstring + "</p>");
            } 
  
            // Display current state, and ON/OFF buttons for slave 2  
            client.println("<p>Slave-2 - " + output2State + "</p>");
            // If the output2State is off, it displays the ON button       
            if (output2State=="off") {
              client.println("<p><a href=\"/2/on\"><button class=\"button\">ON</button></a></p>");
            } else if(output3State=="on"){
              //client.println("<p><a href=\"/3/on\"><button class=\"button\">ON</button></a></p>");
              //client.println("<p>" + dadosstring + "</p>");
            }
            else {
              client.println("<p><a href=\"/2/off\"><button class=\"button button2\">OFF</button></a></p>");
              client.println("<p>" + dadosstring + "</p>");
            }

            // Display current state, and ON/OFF buttons for slave 1 and 2   
            client.println("<p>Slave 1 and 2 - " + output3State + "</p>");
            // If the output3State is off, it displays the ON button       
            if (output3State=="off") {   
              client.println("<p><a href=\"/3/on\"><button class=\"button\">ON</button></a></p>");
            } else {
              client.println("<p><a href=\"/3/off\"><button class=\"button button2\">OFF</button></a></p>");
              client.println("<p>Slave 1: " + dadosstring1 + "</p>");
              client.println("<p>Slave 2: " + dadosstring2 + "</p>");
            }

            // Display current state, and ON/OFF buttons for none  
            client.println("<p>Stop - " + output0State + "</p>");
            // If the output0State is off, it displays the ON button       
            if (output0State=="off") {
              client.println("<p><a href=\"/0/on\"><button class=\"button\">ON</button></a></p>");
            } else {
              client.println("<p><a href=\"/0/off\"><button class=\"button button2\">OFF</button></a></p>");
            }
            
            client.println("</body></html>");

            
            
            // The HTTP response ends with another blank line
            client.println();
            // Break out of the while loop
            break;
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
   
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  } 

  
  else


  
  {
    delay(500);
   
    digitalWrite(23, HIGH);
    if (Serial.available() > 0) {
      j=0;
      while(Serial.available()){
        x[j] = Serial.read();     //Deve ser passado um numero de 5 digitos pela serial read.
                                  // o primeiro será o numero do slave, e o resto será o SP
        j++;
      }
      
      
    }
    if(x[0]=='1'|| x[0]=='2'|| x[0]=='0')
      {
        auxiliar=0;
        FrameEnv[1]=x[0];
        FrameEnv[2]=x[1];
        FrameEnv[3]=x[2];
        FrameEnv[4]=x[3];
        FrameEnv[5]=x[4];
      }
      
    if(x[0]=='3' || auxiliar==1){
                auxiliar=1;
                FrameEnv[2]=x[1];
                FrameEnv[3]=x[2];
                FrameEnv[4]=x[3];
                FrameEnv[5]=x[4];
                if(FrameEnv[1]=='0'){FrameEnv[1]='2';} 
                else if(FrameEnv[1]=='1'){FrameEnv[1]='2';} 
                else if(FrameEnv[1]=='2'){FrameEnv[1]='1';}    
    }
    delay(10);
    if(FrameEnv[1]=='1' || FrameEnv[1]=='2') 
    {
      
      mySerial2.print(FrameEnv);
      delay(10);
      digitalWrite(23, LOW);
      delay(20);
  
      if(mySerial2.available()){
        i=0;
        while (mySerial2.available()) {
          receb[i]=((mySerial2.read()));
          i++;
        }
        if(receb[0]== FrameEnv[1] && receb[1]=='0')
        {
            Serial.println();
            Serial.print("Resposta do dispositivo " );
            Serial.print(FrameEnv[1]);
            Serial.print(": ");
           
            dadosReceb[0]=receb[2]; dadosReceb[1]=receb[3]; dadosReceb[2]=receb[4]; dadosReceb[3]=receb[5];
            int in = ((float)atof(dadosReceb));
            Serial.print(in);
        }
      }    
    }
  }
}
